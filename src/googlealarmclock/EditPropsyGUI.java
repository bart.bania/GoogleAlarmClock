package googlealarmclock;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;
import java.util.TimeZone;
import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

/**
 *
 * @author Bartlomiej Bania
 */
public class EditPropsyGUI extends javax.swing.JFrame {

    static Properties propsy = new Properties();
    static FileInputStream in = null;
    static OutputStream out = null;
    static File props = new File("propsy.properties");
    private String[] timezoneIDs;
    static String TIMEZONE_ID_PREFIXES = "^(Africa|America|Asia|Atlantic|Australia|Europe|Indian|Pacific)/.*";
    ArrayList<String> list;

    public EditPropsyGUI() {
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "Cancel");
        getRootPane().getActionMap().put("Cancel", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        initComponents();
        LoadTZ();
        Timezone.setModel(new DefaultComboBoxModel<>(list.toArray(new String[list.size()])));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        path_chooser = new javax.swing.JFileChooser();
        jPanel1 = new javax.swing.JPanel();
        email_tf = new javax.swing.JTextField();
        pass_tf = new javax.swing.JPasswordField();
        query_tf = new javax.swing.JTextField();
        path_tf = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        okejka = new javax.swing.JButton();
        pass_shower = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        Timezone = new javax.swing.JComboBox();

        path_chooser.setApproveButtonText("Choose");
        path_chooser.setCurrentDirectory(null);
        path_chooser.setDialogTitle("Choose directory");
        path_chooser.setFileSelectionMode(javax.swing.JFileChooser.DIRECTORIES_ONLY);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Edit properties");
        setLocationByPlatform(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(485, 287));

        email_tf.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        email_tf.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                email_tfFocusGained(evt);
            }
        });

        pass_tf.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        pass_tf.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                pass_tfFocusGained(evt);
            }
        });

        query_tf.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        path_tf.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        path_tf.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                path_tfMouseClicked(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("DejaVu Sans", 1, 12)); // NOI18N
        jLabel1.setText("Google account email:");

        jLabel2.setFont(new java.awt.Font("DejaVu Sans", 1, 12)); // NOI18N
        jLabel2.setText("Application-specific password:");

        jLabel3.setFont(new java.awt.Font("DejaVu Sans", 1, 12)); // NOI18N
        jLabel3.setText("Google Calendar query:");

        jLabel4.setFont(new java.awt.Font("DejaVu Sans", 1, 12)); // NOI18N
        jLabel4.setText("Music directory path:");

        okejka.setText("OK");
        okejka.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okejkaActionPerformed(evt);
            }
        });

        pass_shower.setText("show password");
        pass_shower.setFocusable(false);
        pass_shower.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        pass_shower.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pass_showerActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("DejaVu Sans", 1, 12)); // NOI18N
        jLabel5.setText("Your TimeZone:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(okejka, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(email_tf)
                            .addComponent(query_tf)
                            .addComponent(path_tf)
                            .addComponent(pass_shower, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(pass_tf)
                            .addComponent(Timezone, 0, 200, Short.MAX_VALUE))))
                .addGap(31, 31, 31))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(email_tf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pass_tf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pass_shower)
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(query_tf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(path_tf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(Timezone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(okejka, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void LoadTZ() {
        list = new ArrayList<>();
        timezoneIDs = TimeZone.getAvailableIDs();
        for (String TZ : timezoneIDs) {
            if (TZ.matches(TIMEZONE_ID_PREFIXES)) {
                list.add(TZ);
            }
        }
        Collections.sort(list);
    }

    private void path_tfMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_path_tfMouseClicked
        int returnVal = path_chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            String directory = path_chooser.getSelectedFile().toString();
            path_tf.setText(directory);
        }
    }//GEN-LAST:event_path_tfMouseClicked

    private void okejkaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okejkaActionPerformed
        if (email_tf.getText().isEmpty() || pass_tf.getText().isEmpty() || query_tf.getText().isEmpty() || path_tf.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Insert all the data, please!", "Error!", JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                out = new FileOutputStream(props);
                String cron = "* * * * *";
                String mail = email_tf.getText();
                char[] passw = pass_tf.getPassword();
                String quer = query_tf.getText();
                String mp3path = path_tf.getText();
                String tz = Timezone.getSelectedItem().toString();
                String pass = new String(passw);

                StandardPBEStringEncryptor passwordEncryptor = new StandardPBEStringEncryptor();
                passwordEncryptor.setPassword("sialababamak");
                String encryptedPassword = passwordEncryptor.encrypt(pass);

                propsy.setProperty("email", mail);
                propsy.setProperty("password", encryptedPassword);
                propsy.setProperty("query", quer);
                propsy.setProperty("mp3_path", mp3path);
                propsy.setProperty("time_zone", tz);
                propsy.setProperty("scheduler", cron);

                propsy.store(out, null);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                        "Something went wrong!", JOptionPane.WARNING_MESSAGE);
            } finally {
                if (out != null) {
                    try {
                        out.close();
                        JOptionPane.showMessageDialog(null, "Your properties have been saved in:\n" + props.getAbsolutePath() + "\nfile.", "Success!", JOptionPane.INFORMATION_MESSAGE);
                        email_tf.setText("");
                        pass_tf.setText("");
                        query_tf.setText("");
                        path_tf.setText("");
                        dispose();
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                                "Something went wrong!", JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        }
    }//GEN-LAST:event_okejkaActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        this.setLocationRelativeTo(null);
        try {
            in = new FileInputStream(props);
            propsy.load(in);
            String mail = propsy.getProperty("email");
            String pass = propsy.getProperty("password");
            String query = propsy.getProperty("query");
            String path = propsy.getProperty("mp3_path");
            String tz = propsy.getProperty("time_zone");

            StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
            encryptor.setPassword("sialababamak");
            String decryptedPass = encryptor.decrypt(pass);

            email_tf.setText(mail);
            pass_tf.setText(decryptedPass);
            query_tf.setText(query);
            path_tf.setText(path);
            Timezone.setSelectedItem(tz);
            in.close();

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                    "Something went wrong!", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_formWindowOpened

    private void pass_showerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pass_showerActionPerformed
        if (pass_shower.isSelected()) {
            char c = 0;
            pass_tf.setEchoChar(c);
            pass_shower.setText("hide password");
        }
        if (pass_shower.isSelected() != true) {
            char c = '*';
            pass_tf.setEchoChar(c);
            pass_shower.setText("show password");
        }
    }//GEN-LAST:event_pass_showerActionPerformed

    private void email_tfFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_email_tfFocusGained
        email_tf.selectAll();
    }//GEN-LAST:event_email_tfFocusGained

    private void pass_tfFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pass_tfFocusGained
        pass_tf.selectAll();
    }//GEN-LAST:event_pass_tfFocusGained

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EditPropsyGUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EditPropsyGUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EditPropsyGUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EditPropsyGUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new EditPropsyGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox Timezone;
    private javax.swing.JTextField email_tf;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton okejka;
    private javax.swing.JRadioButton pass_shower;
    private javax.swing.JPasswordField pass_tf;
    private javax.swing.JFileChooser path_chooser;
    private javax.swing.JTextField path_tf;
    private javax.swing.JTextField query_tf;
    // End of variables declaration//GEN-END:variables
}
