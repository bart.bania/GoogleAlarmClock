package googlealarmclock.objects;

/**
 *
 * @author Bartlomiej Bania
 */
public enum Action {

    MOVE, DELETE, EXECUTE, READ,;
}
