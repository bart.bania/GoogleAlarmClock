package googlealarmclock.parsers;

import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Bartlomiej Bania
 */
public class DownloaderXMLParserHandler extends DefaultHandler {

    private String currentelement = "";
    private ArrayList<String> files = new ArrayList<String>();

    public DownloaderXMLParserHandler() {
        super();
    }

    @Override
    public void startElement(String uri, String name,
            String qName, Attributes atts) {
        currentelement = qName;
    }

    @Override
    public void characters(char ch[], int start, int length) {
        String value = null;
        if (!currentelement.equals("")) {
            value = String.copyValueOf(ch, start, length).trim();
        }

        if (currentelement.equals("file")) {
            files.add(value);
        }
        currentelement = "";

    }

    public ArrayList<String> getFiles() {
        return files;
    }
}
