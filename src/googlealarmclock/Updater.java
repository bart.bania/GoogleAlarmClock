package googlealarmclock;

import googlealarmclock.objects.Instruction;
import googlealarmclock.objects.Modes;
import googlealarmclock.parsers.UpdateXMLParser;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import org.xml.sax.SAXException;

/**
 *
 * @author Bartlomiej Bania
 */
public class Updater {

    public void update(String instructionsxml, String tmp, Modes mode) throws SAXException,
            FileNotFoundException, IOException, InterruptedException {

        UpdateXMLParser parser = new UpdateXMLParser();
        Iterator iterator = parser.parse(tmp + File.separator + instructionsxml, mode).iterator();
        Instruction instruction;

        while (iterator.hasNext()) {
            instruction = (Instruction) iterator.next();
            switch (instruction.getAction()) {
                case MOVE:
                    copy(tmp + File.separator + instruction.getFilename(), instruction.getDestination());
                    break;
                case DELETE:
                    delete(instruction.getDestination());
                    break;
                case EXECUTE:
                    Runtime.getRuntime().exec("java -jar " + tmp + File.separator + instruction.getFilename());
                    break;
                case READ:
                    File tmpp = new File(tmp + File.separator + instruction.getFilename());
                    String os = System.getProperty("os.name");
                    if (os.contains("Windows")) {
                        Desktop.getDesktop().open(tmpp);
                    } else {
                        Runtime.getRuntime().exec("xdg-open " + tmp + File.separator + instruction.getFilename());
                    }
                    break;
            }
        }

    }

    public void update(String instructionsxml, String tmp, String dstdir, Modes mode) throws SAXException,
            FileNotFoundException, IOException, InterruptedException {

        UpdateXMLParser parser = new UpdateXMLParser();
        Iterator iterator = parser.parse(tmp + File.separator + instructionsxml, mode).iterator();
        Instruction instruction;

        while (iterator.hasNext()) {
            instruction = (Instruction) iterator.next();
            switch (instruction.getAction()) {
                case MOVE:
                    copy(tmp + File.separator + instruction.getFilename(), dstdir + File.separator + instruction.getDestination());
                    break;
                case DELETE:
                    delete(instruction.getDestination());
                    break;
                case EXECUTE:
                    Runtime.getRuntime().exec("java -jar " + tmp + File.separator + instruction.getFilename());
                    break;
                case READ:
                    File tmpp = new File(tmp + File.separator + instruction.getFilename());
                    String os = System.getProperty("os.name");
                    if (os.contains("Windows")) {
                        Desktop.getDesktop().open(tmpp);
                    } else {
                        Runtime.getRuntime().exec("xdg-open " + tmp + File.separator + instruction.getFilename());
                    }
                    break;
            }
        }

    }

    private void copy(String source, String destination) throws FileNotFoundException, IOException {
        File srcfile = new File(source);
        File dstfile = new File(destination);

        InputStream in = new FileInputStream(srcfile);
        OutputStream out = new FileOutputStream(dstfile);

        byte[] buffer = new byte[512];
        int length;

        while ((length = in.read(buffer)) > 0) {
            out.write(buffer, 0, length);
        }

        in.close();
        out.close();
    }

    private void delete(String filename) {
        File file = new File(filename);
        file.delete();
    }
}
