package googlealarmclock;

import com.google.gdata.client.calendar.CalendarService;
import com.google.gdata.data.DateTime;
import com.google.gdata.data.PlainTextConstruct;
import com.google.gdata.data.calendar.CalendarEventEntry;
import com.google.gdata.data.extensions.Recurrence;
import com.google.gdata.data.extensions.When;
import com.google.gdata.util.ServiceException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.KeyStroke;
import javax.swing.SpinnerDateModel;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

/**
 *
 * @author Bartlomiej Bania
 */
public class AddEventGUI extends javax.swing.JFrame {

    DefaultComboBoxModel timeModel1 = new DefaultComboBoxModel(new String[]{"00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"});
    DefaultComboBoxModel timeModel2 = new DefaultComboBoxModel(new String[]{"00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"});
    static Properties propsy = new Properties();
    static FileInputStream in = null;
    static File props = new File("propsy.properties");
    SimpleDateFormat sdfdata = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat sdfdat = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat sdfday = new SimpleDateFormat("EE");
    SimpleDateFormat hrs = new SimpleDateFormat("HH");
    SimpleDateFormat min = new SimpleDateFormat("mm");
    ArrayList<String> list = new ArrayList<>();

    public AddEventGUI() {
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "Cancel");
        getRootPane().getActionMap().put("Cancel", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        initComponents();

        SpinnerDateModel hr = new SpinnerDateModel();
        hr.setCalendarField(Calendar.HOUR_OF_DAY);
        SpinnerDateModel mn = new SpinnerDateModel();
        mn.setCalendarField(Calendar.MINUTE);
        SpinnerDateModel hr2 = new SpinnerDateModel();
        hr2.setCalendarField(Calendar.HOUR_OF_DAY);
        SpinnerDateModel mn2 = new SpinnerDateModel();
        mn2.setCalendarField(Calendar.MINUTE);
        timestart_HR.setModel(hr);
        timestart_HR.setEditor(new JSpinner.DateEditor(timestart_HR, "HH"));
        timestart_MIN.setModel(mn);
        timestart_MIN.setEditor(new JSpinner.DateEditor(timestart_MIN, "mm"));
        timestop_HR.setModel(hr2);
        timestop_HR.setEditor(new JSpinner.DateEditor(timestop_HR, "HH"));
        timestop_MIN.setModel(mn2);
        timestop_MIN.setEditor(new JSpinner.DateEditor(timestop_MIN, "mm"));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        repeat_interval = new javax.swing.JFrame();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        startson_tf = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        monday = new javax.swing.JCheckBox();
        tuesday = new javax.swing.JCheckBox();
        wednesday = new javax.swing.JCheckBox();
        thursday = new javax.swing.JCheckBox();
        friday = new javax.swing.JCheckBox();
        saturday = new javax.swing.JCheckBox();
        sunday = new javax.swing.JCheckBox();
        endsDate = new com.toedter.calendar.JDateChooser();
        jPanel3 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        cancel_repeat = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        choose_from = new com.toedter.calendar.JDateChooser();
        choose_to = new com.toedter.calendar.JDateChooser();
        title_tf = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        repeater = new javax.swing.JCheckBox();
        jButton2 = new javax.swing.JButton();
        timestart_HR = new javax.swing.JSpinner();
        timestart_MIN = new javax.swing.JSpinner();
        timestop_HR = new javax.swing.JSpinner();
        timestop_MIN = new javax.swing.JSpinner();
        jLabel2 = new javax.swing.JLabel();

        repeat_interval.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        repeat_interval.setTitle("Repeat...");
        repeat_interval.setMinimumSize(new java.awt.Dimension(430, 212));
        repeat_interval.setPreferredSize(new java.awt.Dimension(430, 212));
        repeat_interval.setResizable(false);
        repeat_interval.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                repeat_intervalWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                repeat_intervalWindowOpened(evt);
            }
        });
        repeat_interval.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setMinimumSize(new java.awt.Dimension(430, 127));
        jPanel2.setPreferredSize(new java.awt.Dimension(430, 127));

        jLabel5.setFont(new java.awt.Font("DejaVu Sans", 0, 11)); // NOI18N
        jLabel5.setText("Repeat on:");

        jLabel6.setFont(new java.awt.Font("DejaVu Sans", 0, 11)); // NOI18N
        jLabel6.setText("Starts on:");

        startson_tf.setEditable(false);
        startson_tf.setFont(new java.awt.Font("DejaVu Sans", 0, 11)); // NOI18N
        startson_tf.setFocusable(false);
        startson_tf.setRequestFocusEnabled(false);

        jLabel7.setFont(new java.awt.Font("DejaVu Sans", 0, 11)); // NOI18N
        jLabel7.setText("Ends:");

        monday.setFont(new java.awt.Font("DejaVu Sans", 0, 11)); // NOI18N
        monday.setText("M");
        monday.setIconTextGap(2);

        tuesday.setFont(new java.awt.Font("DejaVu Sans", 0, 11)); // NOI18N
        tuesday.setText("T");
        tuesday.setIconTextGap(2);

        wednesday.setFont(new java.awt.Font("DejaVu Sans", 0, 11)); // NOI18N
        wednesday.setText("W");
        wednesday.setIconTextGap(2);

        thursday.setFont(new java.awt.Font("DejaVu Sans", 0, 11)); // NOI18N
        thursday.setText("T");
        thursday.setIconTextGap(2);

        friday.setFont(new java.awt.Font("DejaVu Sans", 0, 11)); // NOI18N
        friday.setText("F");
        friday.setIconTextGap(2);

        saturday.setFont(new java.awt.Font("DejaVu Sans", 0, 11)); // NOI18N
        saturday.setText("S");
        saturday.setIconTextGap(2);

        sunday.setFont(new java.awt.Font("DejaVu Sans", 0, 11)); // NOI18N
        sunday.setText("S");
        sunday.setIconTextGap(2);

        endsDate.setBackground(new java.awt.Color(255, 255, 255));
        endsDate.setDateFormatString("yyyy-MM-dd");
        endsDate.setFont(new java.awt.Font("DejaVu Sans", 0, 11)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(67, 67, 67)
                        .addComponent(monday)
                        .addGap(6, 6, 6)
                        .addComponent(tuesday)
                        .addGap(6, 6, 6)
                        .addComponent(wednesday)
                        .addGap(6, 6, 6)
                        .addComponent(thursday)
                        .addGap(6, 6, 6)
                        .addComponent(friday)
                        .addGap(6, 6, 6)
                        .addComponent(saturday)
                        .addGap(6, 6, 6)
                        .addComponent(sunday))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addGap(71, 71, 71)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(startson_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(endsDate, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(38, 38, 38))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel5))
                    .addComponent(monday)
                    .addComponent(tuesday)
                    .addComponent(wednesday)
                    .addComponent(thursday)
                    .addComponent(friday)
                    .addComponent(saturday)
                    .addComponent(sunday))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel6))
                    .addComponent(startson_tf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel7))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(endsDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(20, 20, 20))
        );

        repeat_interval.getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setMinimumSize(new java.awt.Dimension(430, 65));

        jButton1.setText("Done");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        cancel_repeat.setText("Cancel");
        cancel_repeat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancel_repeatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(130, 130, 130)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(cancel_repeat, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(120, 120, 120))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cancel_repeat, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );

        repeat_interval.getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 127, -1, -1));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Add Alarm");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(425, 207));

        choose_from.setDateFormatString("yyyy-MM-dd");
        choose_from.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                choose_fromPropertyChange(evt);
            }
        });

        choose_to.setDateFormatString("yyyy-MM-dd");

        title_tf.setText("Untitled event");

        jLabel1.setText("to");

        repeater.setMnemonic('r');
        repeater.setText("Repeat");
        repeater.setToolTipText("Alt+R");
        repeater.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repeaterActionPerformed(evt);
            }
        });

        jButton2.setText("Add");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(title_tf)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(choose_from, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel1))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(timestart_HR, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(timestart_MIN, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(timestop_HR, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(timestop_MIN, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(choose_to, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(repeater, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(20, 20, 20))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(title_tf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(choose_from, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(choose_to, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(timestart_HR, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(timestart_MIN, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(timestop_HR, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(timestop_MIN, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(repeater)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        this.setLocationRelativeTo(null);
        try {
            in = new FileInputStream(props);
            propsy.load(in);

            String query = propsy.getProperty("query");

            title_tf.setText(query);
            title_tf.requestFocus();
            title_tf.selectAll();

            Date data = new Date();
            choose_from.setDate(data);
            choose_to.setDate(data);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                    "Something went wrong!", JOptionPane.WARNING_MESSAGE);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                        "Something went wrong!", JOptionPane.WARNING_MESSAGE);
            }
        }
    }//GEN-LAST:event_formWindowOpened

    private void repeaterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repeaterActionPerformed
        if (repeater.isSelected()) {
            repeat_interval.setLocationRelativeTo(null);
            repeat_interval.setVisible(true);
        }
        if (!repeater.isSelected()) {
            clearFields();
        }
    }//GEN-LAST:event_repeaterActionPerformed

    private void cancel_repeatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancel_repeatActionPerformed
        repeat_interval.dispose();
        clearFields();
    }//GEN-LAST:event_cancel_repeatActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            in = new FileInputStream(props);
            propsy.load(in);

            String mail = propsy.getProperty("email");
            String pass = propsy.getProperty("password");
            String timeZone = propsy.getProperty("time_zone");

            Date dataod = choose_from.getDate();
            Date datado = choose_to.getDate();

            StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
            encryptor.setPassword("sialababamak");
            String decryptedPass = encryptor.decrypt(pass);

            CalendarService myService = new CalendarService("SimpleGoogleAlarmClock");
            myService.setUserCredentials(mail, decryptedPass);

            URL postUrl = new URL("http://www.google.com/calendar/feeds/default/private/full");

            CalendarEventEntry myEntry = new CalendarEventEntry();
            myEntry.setTitle(new PlainTextConstruct(title_tf.getText()));

            if (repeater.isSelected()) {
                try {
                    String days = list.toString().replace("[", "").replace("]", "").replace(" ", "");
                    String unt = sdfdat.format(endsDate.getDate());

                    String czasod2 = hrs.format(timestart_HR.getValue()) + min.format(timestart_MIN.getValue()) + "00";
                    String czasdo2 = hrs.format(timestop_HR.getValue()) + min.format(timestop_MIN.getValue()) + "00";
                    String stTime = sdfdat.format(choose_from.getDate()) + "T" + czasod2;
                    String enTime = sdfdat.format(choose_to.getDate()) + "T" + czasdo2;
                    String recurData = "DTSTART;TZID=" + timeZone + ":" + stTime + "\r\nDTEND;TZID=" + timeZone + ":" + enTime + "\r\nRRULE:FREQ=WEEKLY;BYDAY=" + days + ";UNTIL=" + unt;
                    Recurrence r = new Recurrence();
                    r.setValue(recurData);
                    myEntry.setRecurrence(r);

                    CalendarEventEntry insertedEntry = myService.insert(postUrl, myEntry);
                    insertedEntry.update();

                    JOptionPane.showMessageDialog(null, "Succesfully added recurring event:\n" + title_tf.getText()
                            + "\nStarting on: " + sdfdata.format(choose_from.getDate()) + " " + hrs.format(timestart_HR.getValue()) + ":" + min.format(timestart_MIN.getValue())
                            + "\nEnding on: " + sdfdata.format(choose_to.getDate()) + " " + hrs.format(timestop_HR.getValue()) + ":" + min.format(timestop_MIN.getValue())
                            + "\nRepeated until: " + sdfdata.format(endsDate.getDate()) + "\nOn: " + list.toString().replace("[", "").replace("]", ""),
                            "Success!", JOptionPane.INFORMATION_MESSAGE);
                } catch (IOException | ServiceException ex) {
                    JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                            "Something went wrong!", JOptionPane.WARNING_MESSAGE);
                }
            }
            if (!repeater.isSelected()) {
                String dod = sdfdata.format(dataod);
                String ddo = sdfdata.format(datado);
                String czasod = hrs.format(timestart_HR.getValue()) + ":" + min.format(timestart_MIN.getValue()) + ":00";
                String czasdo = hrs.format(timestop_HR.getValue()) + ":" + min.format(timestop_MIN.getValue()) + ":00";

                DateTime startTime = DateTime.parseDateTime(dod + "T" + czasod);
                DateTime endTime = DateTime.parseDateTime(ddo + "T" + czasdo);
                When eventTimes = new When();
                eventTimes.setStartTime(startTime);
                eventTimes.setEndTime(endTime);
                myEntry.addTime(eventTimes);

                CalendarEventEntry insertedEntry = myService.insert(postUrl, myEntry);
                insertedEntry.update();
            }
        } catch (ServiceException | IOException | NumberFormatException ex) {
            JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                    "Something went wrong!", JOptionPane.WARNING_MESSAGE);
        } finally {
            try {
                in.close();
                repeater.setSelected(false);
                clearFields();
                dispose();
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                        "Something went wrong!", JOptionPane.WARNING_MESSAGE);
            }
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void repeat_intervalWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_repeat_intervalWindowClosing
        repeat_interval.dispose();
        clearFields();
    }//GEN-LAST:event_repeat_intervalWindowClosing

    private void choose_fromPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_choose_fromPropertyChange
        Date x = choose_from.getDate();
        choose_to.setDate(x);
    }//GEN-LAST:event_choose_fromPropertyChange

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        repeat_interval.setVisible(false);
        repeater.setSelected(true);
        WeekDays();

        String days = list.toString().replace("[", "").replace("]", "");
        String[] weekds = {"MO", "TU", "WE", "TH", "FR"};
        String weekdays = Arrays.toString(weekds).replace("[", "").replace("]", "");
        String[] alldys = {"MO", "TU", "WE", "TH", "FR", "SA", "SU"};
        String alldays = Arrays.toString(alldys).replace("[", "").replace("]", "");
        if (days.equals(weekdays)) {
            days = "working days";
        }
        if (days.equals(alldays)) {
            days = "all days";
        }
        String stTime = sdfdata.format(choose_from.getDate());
        String enTime = sdfdata.format(endsDate.getDate());
        repeater.setText("Repeat from: " + stTime + " until: " + enTime);
        jLabel2.setText("on: " + days);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void WeekDays() {
        if (monday.isSelected()) {
            list.add("MO");
        }
        if (tuesday.isSelected()) {
            list.add("TU");
        }
        if (wednesday.isSelected()) {
            list.add("WE");
        }
        if (thursday.isSelected()) {
            list.add("TH");
        }
        if (friday.isSelected()) {
            list.add("FR");
        }
        if (saturday.isSelected()) {
            list.add("SA");
        }
        if (sunday.isSelected()) {
            list.add("SU");
        }
        if (!monday.isSelected()) {
            list.remove("MO");
        }
        if (!tuesday.isSelected()) {
            list.remove("TU");
        }
        if (!wednesday.isSelected()) {
            list.remove("WE");
        }
        if (!thursday.isSelected()) {
            list.remove("TH");
        }
        if (!friday.isSelected()) {
            list.remove("FR");
        }
        if (!saturday.isSelected()) {
            list.remove("SA");
        }
        if (!sunday.isSelected()) {
            list.remove("SU");
        }
    }
    private void repeat_intervalWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_repeat_intervalWindowOpened
        clearFields();
    }//GEN-LAST:event_repeat_intervalWindowOpened

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        dispose();
    }//GEN-LAST:event_formWindowClosing

    private void clearFields() {
        try {
            jLabel2.setText("");
            repeater.setText("Repeat");
            repeater.setSelected(false);
            list.clear();
            startson_tf.setText("");
            monday.setSelected(false);
            tuesday.setSelected(false);
            wednesday.setSelected(false);
            thursday.setSelected(false);
            friday.setSelected(false);
            sunday.setSelected(false);
            saturday.setSelected(false);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            org.joda.time.DateTime datPlus = new org.joda.time.DateTime(choose_from.getDate()).plusDays(7);
            String da = datPlus.toString();
            Date endDate = sdf.parse(da);
            endsDate.setDate(endDate);
            String dod = sdfdata.format(choose_from.getDate());
            String dod2 = sdfday.format(choose_from.getDate());
            startson_tf.setText(dod);
            if (dod2.matches("Mon")) {
                monday.setSelected(true);
            }
            if (dod2.matches("Tue")) {
                tuesday.setSelected(true);
            }
            if (dod2.matches("Wed")) {
                wednesday.setSelected(true);
            }
            if (dod2.matches("Thu")) {
                thursday.setSelected(true);
            }
            if (dod2.matches("Fri")) {
                friday.setSelected(true);
            }
            if (dod2.matches("Sat")) {
                saturday.setSelected(true);
            }
            if (dod2.matches("Sun")) {
                sunday.setSelected(true);
            }
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                    "Something went wrong!", JOptionPane.WARNING_MESSAGE);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AddEventGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AddEventGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AddEventGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AddEventGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new AddEventGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancel_repeat;
    private com.toedter.calendar.JDateChooser choose_from;
    private com.toedter.calendar.JDateChooser choose_to;
    private com.toedter.calendar.JDateChooser endsDate;
    private javax.swing.JCheckBox friday;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JCheckBox monday;
    private javax.swing.JFrame repeat_interval;
    private javax.swing.JCheckBox repeater;
    private javax.swing.JCheckBox saturday;
    private javax.swing.JTextField startson_tf;
    private javax.swing.JCheckBox sunday;
    private javax.swing.JCheckBox thursday;
    private javax.swing.JSpinner timestart_HR;
    private javax.swing.JSpinner timestart_MIN;
    private javax.swing.JSpinner timestop_HR;
    private javax.swing.JSpinner timestop_MIN;
    private javax.swing.JTextField title_tf;
    private javax.swing.JCheckBox tuesday;
    private javax.swing.JCheckBox wednesday;
    // End of variables declaration//GEN-END:variables
}
