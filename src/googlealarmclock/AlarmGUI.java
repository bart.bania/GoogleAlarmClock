package googlealarmclock;

import com.google.gdata.client.calendar.CalendarQuery;
import com.google.gdata.client.calendar.CalendarService;
import com.google.gdata.data.calendar.CalendarEventEntry;
import com.google.gdata.data.calendar.CalendarEventFeed;
import com.google.gdata.util.ServiceException;
import googlealarmclock.objects.Modes;
import googlealarmclock.objects.Release;
import googlealarmclock.parsers.ReleaseXMLParser;
import it.sauronsoftware.cron4j.InvalidPatternException;
import it.sauronsoftware.cron4j.Scheduler;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.xml.sax.SAXException;

/**
 *
 * @author Bartlomiej Bania
 */
public class AlarmGUI extends javax.swing.JFrame {

    static Properties propsy = new Properties();
    static FileInputStream in = null;
    static File props = new File("propsy.properties");
    static SimpleDateFormat datetajm = new SimpleDateFormat("yyyy-MM-dd   HH:mm");
    static int currentSecond;
    static Calendar calendar;

    public AlarmGUI() {
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "Cancel");
        getRootPane().getActionMap().put("Cancel", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        initComponents();
        this.setLocationRelativeTo(null);

        int answer = -1;
        Release release = new Release();
        release.setpkgver("1.0");
        release.setPkgrel("11");
        ReleaseXMLParser parser = new ReleaseXMLParser();
        String fajl = new File("").getAbsolutePath();
        try {
            Release current = parser.parse("http://www.bartbania.com/GAC/updater/latest.xml", Modes.URL);
            if (current.compareTo(release) > 0) {
                answer = JOptionPane.showConfirmDialog(rootPane, "A new version of\n"
                        + "Google Alarm Clock\nis available.\nWould you like to install it now?",
                        "Update", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
                switch (answer) {
                    case 0:
                        Downloader dl = new Downloader();
                        dl.download("http://www.bartbania.com/GAC/updater/files.xml", fajl + File.separator + "tmp", Modes.URL);
                        break;
                    case 1:
                        break;
                }
            }
        } catch (SAXException ex) {
            JOptionPane.showMessageDialog(rootPane, "The xml wasn't loaded succesfully!\n",
                    "Something went wrong!", JOptionPane.WARNING_MESSAGE);
            answer = -1;
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(rootPane, "Files were unable to be read or created successfully!\n"
                    + "Please be sure that you have the right permissions and internet connectivity!",
                    "Something went wrong!", JOptionPane.WARNING_MESSAGE);
            answer = -1;
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(rootPane, "IOEXception!",
                    "Something went wrong!", JOptionPane.WARNING_MESSAGE);
            answer = -1;
        } catch (InterruptedException ex) {
            JOptionPane.showMessageDialog(rootPane, "The connection has been lost!\n"
                    + "Please check your internet connectivity!",
                    "Something went wrong!", JOptionPane.WARNING_MESSAGE);
            answer = -1;
        }

        if (answer == 0) {
            try {
                Updater update = new Updater();
                update.update("update.xml", fajl + File.separator + "tmp", Modes.FILE);
                JOptionPane.showMessageDialog(rootPane, "The update was completed successfuly.\n"
                        + "Please restart the application in order the changes take effect.");
            } catch (SAXException | IOException | InterruptedException | HeadlessException ex) {
                JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                        "Something went wrong!", JOptionPane.WARNING_MESSAGE);
            }
        }

        File tmp = new File(fajl + File.separator + "tmp");
        if (tmp.exists()) {
            for (File file : tmp.listFiles()) {
                file.delete();
            }
            tmp.delete();
            System.exit(0);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        show_events = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        queryArea = new javax.swing.JTextArea();
        start_alarm = new javax.swing.JButton();
        edit_props = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        dateLabel = new javax.swing.JLabel();
        add_alarm = new javax.swing.JButton();
        about = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Google Alarm Clock");
        setLocationByPlatform(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));

        show_events.setBackground(new java.awt.Color(153, 153, 255));
        show_events.setMnemonic('e');
        show_events.setText("Show events");
        show_events.setToolTipText("Alt+E");
        show_events.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                show_eventsActionPerformed(evt);
            }
        });

        jScrollPane1.setBorder(null);

        queryArea.setEditable(false);
        queryArea.setBackground(new java.awt.Color(204, 204, 255));
        queryArea.setColumns(15);
        queryArea.setLineWrap(true);
        queryArea.setRows(16);
        queryArea.setTabSize(2);
        queryArea.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder("")));
        jScrollPane1.setViewportView(queryArea);

        start_alarm.setBackground(new java.awt.Color(255, 204, 204));
        start_alarm.setMnemonic('s');
        start_alarm.setText("Start alarm clock");
        start_alarm.setToolTipText("Alt+S");
        start_alarm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                start_alarmActionPerformed(evt);
            }
        });

        edit_props.setBackground(new java.awt.Color(204, 204, 204));
        edit_props.setMnemonic('p');
        edit_props.setText("Edit Properties");
        edit_props.setToolTipText("Alt+P");
        edit_props.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edit_propsActionPerformed(evt);
            }
        });

        jLabel1.setText("Current date:");

        dateLabel.setFont(new java.awt.Font("DejaVu Sans", 1, 12)); // NOI18N
        dateLabel.setRequestFocusEnabled(false);

        add_alarm.setBackground(new java.awt.Color(204, 255, 204));
        add_alarm.setMnemonic('a');
        add_alarm.setText("Add entry");
        add_alarm.setToolTipText("Alt+A");
        add_alarm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                add_alarmActionPerformed(evt);
            }
        });

        about.setBackground(new java.awt.Color(204, 204, 204));
        about.setText("About");
        about.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(start_alarm, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edit_props, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(show_events, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(add_alarm, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(about, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 292, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(dateLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(20, 20, 20))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dateLabel)
                    .addComponent(jLabel1))
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(show_events, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(start_alarm, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(add_alarm, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52)
                        .addComponent(edit_props, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(about, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(20, 20, 20))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        if (!props.exists()) {
            JOptionPane.showMessageDialog(null, "You have to create properties file!", "Error!", JOptionPane.ERROR_MESSAGE);
            PropsyGUI pg = new PropsyGUI();
            pg.setLocationRelativeTo(null);
            pg.setVisible(true);
        }
    }//GEN-LAST:event_formWindowOpened

    private void show_eventsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_show_eventsActionPerformed
        try {
            queryArea.setText("");
            in = new FileInputStream(props);
            propsy.load(in);

            String mail = propsy.getProperty("email");
            String pass = propsy.getProperty("password");
            String query = propsy.getProperty("query");

            StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
            encryptor.setPassword("sialababamak");
            String decryptedPass = encryptor.decrypt(pass);

            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm");
            DateTimeFormatter fmt = DateTimeFormat.forPattern("MM-dd-yyyy HH:mm");
            DateTime sDate = new DateTime();
            String startDate = sDate.toString(fmt);
            String plusDate = new DateTime(sDate).plus(Period.days(10)).toString(fmt);
            Date st = sdf.parse(startDate);
            Date en = sdf.parse(plusDate);

            CalendarService myService = new CalendarService("SimpleGoogleAlarmClock");
            myService.setUserCredentials(mail, decryptedPass);

            URL feedUrl = new URL("http://www.google.com/calendar/feeds/default/private/full");

            com.google.gdata.data.DateTime s = new com.google.gdata.data.DateTime(st);
            com.google.gdata.data.DateTime e = new com.google.gdata.data.DateTime(en);
            CalendarQuery myQuery = new CalendarQuery(feedUrl);
            myQuery.setMinimumStartTime(s);
            myQuery.setMaximumStartTime(e);
            myQuery.setFullTextQuery(query);
            myQuery.setStringCustomParameter("singleevents", "true");
            myQuery.setStringCustomParameter("sortorder", "a");

            CalendarEventFeed myResultsFeed = myService.query(myQuery, CalendarEventFeed.class);
            queryArea.append("Wake up sheduled for:\n");
            for (int i = 0; i < myResultsFeed.getEntries().size(); i++) {
                CalendarEventEntry MatchEntry = (CalendarEventEntry) myResultsFeed.getEntries().get(i);
                String time = MatchEntry.getTimes().get(0).getStartTime().toUiString();
                String pobudka = MatchEntry.getTitle().getPlainText();
                queryArea.append("\t" + time + " " + pobudka + "\n");
            }
        } catch (ServiceException | IOException | ParseException e) {
            JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                    "Something went wrong!", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_show_eventsActionPerformed

    private void start_alarmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_start_alarmActionPerformed
        try {
            queryArea.append("Wake up precedure started.\n");
            Scheduler sched = new Scheduler();
            in = new FileInputStream(props);
            propsy.load(in);
            String coile = propsy.getProperty("scheduler");
            sched.schedule(coile, new Runnable() {
                @Override
                public void run() {
                    try {
                        queryArea.setText("");
                        queryArea.setText("Alarm running...\n");
                        in = new FileInputStream(props);

                        propsy.load(in);
                        String mail = propsy.getProperty("email");
                        String pass = propsy.getProperty("password");
                        String query = propsy.getProperty("query");

                        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
                        encryptor.setPassword("sialababamak");
                        String decryptedPass = encryptor.decrypt(pass);

                        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm");
                        DateTimeFormatter fmt = DateTimeFormat.forPattern("MM-dd-yyyy HH:mm");
                        DateTimeFormatter fmtt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
                        DateTime sDate = new DateTime();
                        String startDate = sDate.toString(fmt);
                        String plusDate = new DateTime(sDate).plus(Period.days(10)).toString(fmt);
                        Date st = sdf.parse(startDate);
                        Date en = sdf.parse(plusDate);

                        CalendarService myService = new CalendarService("SimpleGoogleAlarmClock");
                        myService.setUserCredentials(mail, decryptedPass);

                        URL feedUrl = new URL("http://www.google.com/calendar/feeds/default/private/full");

                        com.google.gdata.data.DateTime s = new com.google.gdata.data.DateTime(st);
                        com.google.gdata.data.DateTime e = new com.google.gdata.data.DateTime(en);
                        s.setTzShift(60);
                        e.setTzShift(60);
                        CalendarQuery myQuery = new CalendarQuery(feedUrl);
                        myQuery.setMinimumStartTime(s);
                        myQuery.setMaximumStartTime(e);
                        myQuery.setFullTextQuery(query);
                        myQuery.setStringCustomParameter("singleevents", "true");
                        myQuery.setStringCustomParameter("sortorder", "a");

                        CalendarEventFeed myResultsFeed = myService.query(myQuery, CalendarEventFeed.class);
                        queryArea.append("Wake up sheduled for:\n");
                        for (int i = 0; i < myResultsFeed.getEntries().size(); i++) {
                            CalendarEventEntry MatchEntry = (CalendarEventEntry) myResultsFeed.getEntries().get(i);
                            String time = MatchEntry.getTimes().get(0).getStartTime().toUiString();
                            String pobudka = MatchEntry.getTitle().getPlainText();
                            queryArea.append("\t" + time + " " + pobudka + "\n");
                            if (time.matches(sDate.toString(fmtt))) {
                                new Thread(alarm).start();
                            }
                        }
                    } catch (ServiceException | IOException | ParseException e) {
                        JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                                "Something went wrong!", JOptionPane.WARNING_MESSAGE);
                    }
                }
            });
            sched.start();
        } catch (IOException | InvalidPatternException | IllegalStateException ex) {
            JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                    "Something went wrong!", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_start_alarmActionPerformed

    private void edit_propsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edit_propsActionPerformed
        EditPropsyGUI epg = new EditPropsyGUI();
        epg.setLocationRelativeTo(null);
        epg.setVisible(true);
    }//GEN-LAST:event_edit_propsActionPerformed

    private void add_alarmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_add_alarmActionPerformed
        AddEventGUI aeg = new AddEventGUI();
        aeg.setVisible(true);
    }//GEN-LAST:event_add_alarmActionPerformed

    private void aboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutActionPerformed
        try {
            final ImageIcon icon = new ImageIcon(new URL("http://1.gravatar.com/avatar/7da3c6b9fdd3714d988aa5a1b0d3824f"));
            JTextArea textarea = new JTextArea();
            Color framecol = new Color(214, 217, 223);
            textarea.setBackground(framecol);
            textarea.setEditable(false);
            textarea.setColumns(7);
            textarea.setBorder(null);
            textarea.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
            textarea.append("Simple Google Alarm Clock\n");
            textarea.append("\n");
            textarea.append("Author: Bartlomiej Bania\n");
            textarea.append("Website: http://www.bartbania.com/");
            textarea.append("\n");
            textarea.append("Contact: contact@bartbania.com");
            textarea.append("\n");
            JOptionPane.showMessageDialog(rootPane, textarea,
                    "About", JOptionPane.INFORMATION_MESSAGE, icon);
        } catch (MalformedURLException ex) {
            JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                    "Something went wrong!", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_aboutActionPerformed

    Thread alarm = new Thread(new Runnable() {
        @Override
        public void run() {
            try {
                in = new FileInputStream(props);
                propsy.load(in);

                String path = propsy.getProperty("mp3_path");

                File dir = new File(path);
                if (!dir.isDirectory()) {
                } else {
                    File[] files = dir.listFiles();
                    File selected = files[new Random().nextInt(files.length)];
                    String empe = selected.toString();
                    String decodedPath = URLDecoder.decode(empe, "UTF-8");
                    try {
                        final String os = System.getProperty("os.name");
                        if (os.contains("Windows")) {
                            queryArea.append("Waking up with: " + decodedPath);
                            String playCommand = "cmd /c start \"%programfiles%\\Windows Media Player\\wmplayer.exe\" " + "\"" + decodedPath + "\"";
                            Runtime.getRuntime().exec(playCommand);
                        } else {
                            queryArea.append("Waking up with: " + decodedPath);
                            String[] playCommand = {"mpg321", "-g", "100", decodedPath};
                            Runtime.getRuntime().exec(playCommand);
                        }
                    } catch (FileNotFoundException ex) {
                        JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                                "Something went wrong!", JOptionPane.WARNING_MESSAGE);
                    }
                }
            } catch (IOException e) {
                JOptionPane.showMessageDialog(rootPane, "Please contact the developer.",
                        "Something went wrong!", JOptionPane.WARNING_MESSAGE);
            }
        }
    });

    static class Clock {

        public void reset() {
            calendar = Calendar.getInstance();
            currentSecond = calendar.get(Calendar.SECOND);
        }

        public void start() {
            reset();
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    if (currentSecond == 60) {
                        reset();
                    }
                    dateLabel.setText(String.format("%s:%02d", datetajm.format(calendar.getTime()), currentSecond));
                    currentSecond++;
                }
            }, 0, 1000);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AlarmGUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AlarmGUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AlarmGUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AlarmGUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new AlarmGUI().setVisible(true);
                Clock clock = new Clock();
                clock.start();
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton about;
    private javax.swing.JButton add_alarm;
    private static javax.swing.JLabel dateLabel;
    private javax.swing.JButton edit_props;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea queryArea;
    private javax.swing.JButton show_events;
    private javax.swing.JButton start_alarm;
    // End of variables declaration//GEN-END:variables
}
